using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStarter : MonoBehaviour
{
    public string musicToStart;

    void Start()
    {
        //Debug.Log(AudioManager.instance);
        AudioManager.instance.PlayMusic(musicToStart);
    }
}
