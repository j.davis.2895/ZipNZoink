﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionScript : MonoBehaviour
{
    [Header("Make sure local right point towards 'next'\n")]
    public GameObject prev;
    public GameObject next;

    public Transform prevSpawnPoint;
    public Transform nextSpawnPoint;

    private Vector2 midpoint;
    private Vector2 toNext;

    private Vector2 transitionRight;

    private void Start()
    {
        midpoint = this.transform.position;
        toNext = (Vector2)next.transform.position - midpoint;
        transitionRight = transform.parent.InverseTransformDirection(this.transform.right);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Vector2 localPlayerPosition = transform.InverseTransformPoint(other.transform.position);
        Vector2 globalPlayerPosition = other.transform.position;

        Vector2 midToPlayerPosition = globalPlayerPosition - midpoint;
        Vector2 projection = Vector3.Project(midToPlayerPosition, toNext.normalized);

        bool goToNext = Vector3.Dot(midToPlayerPosition, transitionRight) > 0 ? true : false;

        SoftSave(goToNext);

        if (goToNext)
        {
            prev.SetActive(false);
            next.SetActive(true);
        }
        else
        {
            prev.SetActive(true);
            next.SetActive(false);
        }
    }

    private void SoftSave(bool toNext)
    {
        if (toNext)
        {
            Instance.currentCameraName = next.name;
            Instance.currentPosition = nextSpawnPoint.position;
        }
        else
        {
            Instance.currentCameraName = prev.name;
            Instance.currentPosition = prevSpawnPoint.position;
        }

        ObjectWatcherScript watcher = GameManager.instance.GetObjectWatcher();

        if(watcher != null)
        {
            for (int i = 0; i < watcher.pickups.Length; i++)
            {
                CoinScript coin = watcher.pickups[i].GetComponent("CoinScript") as CoinScript;
                Instance.pickupState[Instance.currentLevel][i] = coin.coinObject.activeSelf;
            }

            if (DebugOptions.debugText)
                Debug.Log("Soft saved");
        }
    }
}
