﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointScript : MonoBehaviour
{
    public GameObject currentCamera;
    public Transform spawnPoint;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Soft save would be done here
        if (collision.gameObject.tag == "Player")
        {
            Instance.currentCameraName = currentCamera.name;
            Instance.currentPosition = spawnPoint.position;

            ObjectWatcherScript watcher = GameManager.instance.GetObjectWatcher();

            if (watcher.gameObject != null)
            {
                for (int i = 0; i < watcher.pickups.Length; i++)
                {
                    CoinScript coin = watcher.pickups[i].GetComponent("CoinScript") as CoinScript;
                    Instance.pickupState[Instance.currentLevel][i] = coin.coinObject.activeSelf;
                }
            }

            if (DebugOptions.debugText)
                Debug.Log("Soft saved");
        }
    }
}