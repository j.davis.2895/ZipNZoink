﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayTransitionScript : MonoBehaviour
{
    public GameObject next;

    public Transform nextSpawnPoint;

    void OnTriggerExit2D(Collider2D other)
    {
        SoftSave(true);
        next.SetActive(true);
    }

    private void SoftSave(bool toNext)
    {
        Instance.currentCameraName = next.name;
        Instance.currentPosition = nextSpawnPoint.position;

        ObjectWatcherScript watcher = GameManager.instance.GetObjectWatcher();

        if (watcher != null)
        {
            for (int i = 0; i < watcher.pickups.Length; i++)
            {
                CoinScript coin = watcher.pickups[i].GetComponent("CoinScript") as CoinScript;
                Instance.pickupState[Instance.currentLevel][i] = coin.coinObject.activeSelf;
            }

            if (DebugOptions.debugText)
                Debug.Log("Soft saved");
        }
    }
}
