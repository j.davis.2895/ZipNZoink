using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public AudioMixerGroup mixerSFXGroup;
    public AudioMixerGroup mixerMusicGroup;

    public Sound[] sounds;
    public Sound[] music;

    private Sound currentMusic = null;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = mixerSFXGroup;
        }

        foreach (Sound m in music)
        {
            m.source = gameObject.AddComponent<AudioSource>();
            m.source.clip = m.clip;
            m.source.volume = m.volume;
            m.source.pitch = m.pitch;
            m.source.loop = m.loop;
            m.source.outputAudioMixerGroup = mixerMusicGroup;
        }

    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.Log("Sound '" + name + "' not found.");
            return;
        }
        s.source.Play();
    }

    public void PlayMusic(string name)
    {
        if (currentMusic != null && name == currentMusic.name)
        {
            Debug.Log("Music '" + name + "' already playing.");
            return;
        }

        Sound newMusic = Array.Find(music, sound => sound.name == name);
        if (newMusic == null)
        {
            Debug.Log("Music '" + name + "' not found.");
            return;
        }

        currentMusic?.source.Stop();
        currentMusic = newMusic;
        currentMusic.source.Play();
        StartCoroutine(FadeMusicIn());
    }

    IEnumerator FadeMusicIn()
    {
        float targetVolume = currentMusic.source.volume;
        float timeElapsed = 0f;
        float fadeInTime = 3.5f;

        while (timeElapsed < fadeInTime)
        {
            currentMusic.source.volume = Mathf.Lerp(0, targetVolume, timeElapsed/fadeInTime);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        
    }

}
