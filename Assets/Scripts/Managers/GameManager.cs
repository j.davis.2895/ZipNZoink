﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject player;
    public GameObject mainCamera;
    public GameObject newCamera;
    public GameObject active;

    private GameObject watcherObject;

    public GameObject transitionEffectObject;
    public Animator transition;
    public float transitionTime;

    private void Awake()
    {
        //load watchers
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

    }

    //Hopefully this is temporary. Just here because OnLevelWasLoaded isn't called when first hitting play.
    private void Start()
    {
        watcherObject = GameObject.Find("ObjectWatcher");

        string levelName = SceneManager.GetActiveScene().name;
        Instance.currentLevel = levelName;
        if (watcherObject)
        {
            ObjectWatcherScript watcher = watcherObject.GetComponent("ObjectWatcherScript") as ObjectWatcherScript;

            //First time in this level
            if (!Instance.pickupState.ContainsKey(levelName))
            {
                Instance.pickupState.Add(levelName, new List<bool>());
                Instance.checkPoints.Add(levelName, new List<bool>());

                for (int i = 0; i < watcher.pickups.Length; i++)
                {
                    Instance.pickupState[levelName].Add(true);
                }
            }
        }

        player = GameObject.Find("Player");
        mainCamera = GameObject.Find("MainCamera");

        if (player != null)
        {
            transitionEffectObject.transform.position = player.transform.position;
        }
        else
        {
            transitionEffectObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, 1f));
        }
        transitionEffectObject.SetActive(true);
    }

    private void OnLevelWasLoaded()
    {
        // Checks for duplicate manager, stops this entire function from running twice. Singletons, amirite?
        if (this != instance) return;

        watcherObject = GameObject.Find("ObjectWatcher");
        player = GameObject.Find("Player");
        mainCamera = GameObject.Find("MainCamera");

        string levelName = SceneManager.GetActiveScene().name;

        //If level name is different, we're in a different level
        //Reset the checkpoint
        if (Instance.currentLevel != levelName)
        {
            Instance.currentLevel = levelName;
            //Resetting checkpoint
            Instance.currentCameraName = null;
        }

        if (watcherObject)
        {
            ObjectWatcherScript watcher = watcherObject.GetComponent("ObjectWatcherScript") as ObjectWatcherScript;

            //First time in this level
            if (!Instance.pickupState.ContainsKey(levelName))
            {
                Instance.pickupState.Add(levelName, new List<bool>());
                Instance.checkPoints.Add(levelName, new List<bool>());

                for (int i = 0; i < watcher.pickups.Length; i++)
                {
                    Instance.pickupState[levelName].Add(true);
                }
            }
            else
            {
                for (int i = 0; i < watcher.pickups.Length; i++)
                {
                    CoinScript coin = watcher.pickups[i].GetComponent("CoinScript") as CoinScript;
                    if (!Instance.pickupState[levelName][i])
                    {
                        coin.Disable();
                    }
                }
                //Check if a checkpoint has been hit
                if (Instance.currentCameraName != null)
                {
                    watcher.startingCamera.SetActive(false);
                    GameObject checkpintCam = watcher.cameras.Find(pred => pred.name == Instance.currentCameraName);
                    checkpintCam.SetActive(true);
                    player.transform.position = (Vector2)Instance.currentPosition;  //cast as Vector2 to remove Z component
                }
                else
                {
                    Debug.Log("Haven't hit a checkpoint");
                }
            }
        }
        else
        {
            Debug.Log("No object watcher");
        }

        //Debug.Log(player != null);
        if (player != null)
        {
            transitionEffectObject.transform.position = player.transform.position;
            //Debug.Log("Transition In pls");
        }
        else
        {
            transitionEffectObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, 1f));
        }
        transition.SetTrigger("TransitionIn");
    }

    public void LoadLevel(string levelName)
    {
        //Would do hard saving here
        //SceneManager.LoadScene(levelName);
        StartCoroutine(LoadOutTransition(levelName));
    }

    public void RestartLevel()
    {
        StartCoroutine(LoadOutTransition(SceneManager.GetActiveScene().name));
    }

    IEnumerator LoadOutTransition(string levelName)
    {
        if (player != null)
        {
            transitionEffectObject.transform.position = player.transform.position;
            player.GetComponent<PlayerMovement_OneHook>().enabled = false;
            player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
        transition.SetTrigger("TransitionOut");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelName);
    }

    public ObjectWatcherScript GetObjectWatcher()
    {
        if (watcherObject != null)
            return watcherObject.GetComponent("ObjectWatcherScript") as ObjectWatcherScript;
        else
            return null;
    }

    public void Quit()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
