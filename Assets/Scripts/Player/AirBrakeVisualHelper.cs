﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirBrakeVisualHelper : MonoBehaviour
{
    public AnimationCurve scaleOverTime;
    public ParticleSystem particleEffects;
    public Animator smokeAnimator;
    private IEnumerator visualPlayer;

    private void Start()
    {
        particleEffects.Stop();
        smokeAnimator.Play("NoAirBrake");
        //particleEffects.transform.localScale = Vector3.zero;
    }

    public void PlayVisuals(float playTime)
    {
        var dur = particleEffects.main.duration;
        dur = playTime; 
        particleEffects.Play(false);
        smokeAnimator.Play("AirBrake");
        particleEffects.transform.localScale = Vector3.one;
    }

    public void StopVisuals()
    {
        particleEffects.Stop();
        smokeAnimator.Play("NoAirBrake");
        this.StopAllCoroutines();
        //particleEffects.transform.localScale = Vector3.zero;
    }

    IEnumerator PlayingVisuals()
    {
        yield return null;
    }
}
