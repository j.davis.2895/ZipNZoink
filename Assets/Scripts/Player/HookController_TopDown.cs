﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookController_TopDown : MonoBehaviour
{
    // Setup variables
    private GameObject h_Object;        // The hook 'head' that gets fired and is one end of the joint.
    private float maxReelSpeed;         // The maximum speed the player can reach while reeling in a connected hook.
    private float timeToMaxReelSpeed;   // The time it takes to reach the maximum reel speed.
    private float inputReelMinimum;     // The minimum input that needs to be registered before reeling happens (for controller trigger deadzones).
    private float minJointDistance;     // The smallest size the joint can get to.
    private PlayerMovement_TopDown movementController;
    // Physics
    private DistanceJoint2D h_Joint;    // The hinge joint that is the basis of all the swing/reel mechanics.
    private float reelPerSec;           // The amount of 'reeling' to apply per second if reeling is happening. reelPerSec = maxReelSpeed / timeToMaxReelSpeed.
    private float reelToApply;          // Is used to keep track of how much the joint is going to be reducing in size.
    private Rigidbody2D rb;
    // Line drawing
    private GameObject h_LineContainer = null; // The gameobject that contains the line renderer for this hook
    private LineRenderer h_Line;        // The line renderer componenet for this hook
    private Color regularColor = Color.red;
    private Color dashingPossibleColor = Color.cyan;
    public bool allowContRender = true;

    // Hook helpers
    private bool h_out = false;          // Keeps track of if the hook is 'out' of the player
    public bool HookOut
    {
        get { return h_out; }
        protected set { }
    }
    private bool h_onGround = false;     // Keeps track of if the hook is currently connected to the ground
    public bool HookOnGround
    {
        get { return h_onGround; }
        protected set { }
    }
    private bool dashPossible = false;   // Keeps track off if the conditions for hook dashing have been met
    public bool DashPossible
    {
        get { return dashPossible; }
        protected set { }
    }

    // Particle system
    private ParticleSystem line_ps;

    void Start()
    {
        line_ps = GetComponent<ParticleSystem>();
        rb = GetComponent<Rigidbody2D>();
        movementController = GetComponent<PlayerMovement_TopDown>();
    }

    private void OnEnable()
    {
        HookHelper_TopDown.OnHookHitGround += HookHitGround;
        HookHelper_TopDown.OnHookHitHazard += HookHitHazard;
    }

    private void OnDisable()
    {
        HookHelper_TopDown.OnHookHitGround -= HookHitGround;
        HookHelper_TopDown.OnHookHitHazard -= HookHitHazard;
    }

    private void FixedUpdate()
    {
        if (h_onGround)
        {
            // Reduce joint length by reel amount
            float newJointDistance = h_Joint.distance - reelToApply * Time.fixedDeltaTime;
            if (newJointDistance <= minJointDistance)
                h_Joint.distance = minJointDistance;
            else
                h_Joint.distance -= reelToApply * Time.fixedDeltaTime;
            // This next bit is needed to prevent a bug that causes reeling to not work if the player collided with the ground while swinging
            //h_Joint.enabled = false;
            h_Joint.enabled = true;
        }
    }

    private void LateUpdate()
    {
        // Drawing Hook
        dashPossible = false;
        if (h_out)
        {
            h_Line.enabled = true;
            if (h_onGround)
            {
                if (rb.velocity.magnitude >= 20f || 
                    rb.velocity.magnitude >= 10.0f || 
                    (this.transform.position.y > h_Object.transform.position.y - 0.1f) || 
                    movementController?.IsGrounded() == true)
                {
                    dashPossible = false;
                }
                else
                {
                    dashPossible = true;
                }
            }
            DrawHook();
        }
        else
        {
            h_Line.enabled = false;
        }
    }

    public void SetupHook(GameObject newHookObject, HookControllerCommonSetup newHookCommonData)
    {
        // Variables
        h_Object = newHookObject;
        inputReelMinimum = newHookCommonData.inputReelMinimum;
        maxReelSpeed = newHookCommonData.maxReelSpeed;
        timeToMaxReelSpeed = newHookCommonData.timeToMaxReelSpeed;
        minJointDistance = newHookCommonData.minJointDistance;

        // Line (drawing) setup
        if (h_LineContainer == null)
        {
            h_LineContainer = new GameObject("HookLine");
            h_LineContainer.transform.parent = newHookCommonData.controllerParent.transform;
            h_Line = h_LineContainer.AddComponent<LineRenderer>();
        }
        else
        {
            h_Line = h_LineContainer.GetComponent<LineRenderer>();
        }
        h_Line.widthMultiplier = 0.1f;
        h_Line.positionCount = 2;
        h_Line.sortingOrder = -2;

        // Joint setup
        h_Joint = newHookCommonData.controllerParent.AddComponent<DistanceJoint2D>();   // Attach one end of the joint to the player.
        h_Joint.enabled = false;    // Disabled at start by default.
        h_Joint.autoConfigureDistance = false;  // Setting to false allows for changing distances.
        h_Joint.connectedBody = h_Object.GetComponent<Rigidbody2D>();               // Attach the other end of the joint to the hook head.
        h_Joint.maxDistanceOnly = true;     // The joint can be 'compressed' but not 'stretched'. Helps when more than one hook is in use.
       
        // Hook movement setup
        reelPerSec = maxReelSpeed / timeToMaxReelSpeed;
        
        // Other
        PlayerMovement_TopDown.DetachHook.AddListener(DisconnectHook);
        h_Object.SetActive(false);
    }

    public void FireHook(Vector2 firingDirection)
    {
        // Disconnect and fire are two button presses (also works for 'holding' the hook)
        if (DebugOptions.hookFireVarient == HookFireVariant.TwoPress || DebugOptions.hookFireVarient == HookFireVariant.Hold || DebugOptions.hookFireVarient == HookFireVariant.OneHook)
        {
            if (!h_out)
            {   // If the hook isn't out yet, activate the hook and fire it
                if (firingDirection != Vector2.zero)
                {
                    h_out = true;
                    h_Object.SetActive(true);
                    h_Object.GetComponent<HookHelper_TopDown>().FireHook(this.transform.position, firingDirection);
                    if (allowContRender)
                        StartContinuousPartileEffect();
                }
            }
            else if (h_out)
            {   // If the hook is already out
                DisconnectHook();
            }
        }

        // Disconnect and fire are one button press
        if (DebugOptions.hookFireVarient == HookFireVariant.OnePress)
        {
            DisconnectHook();
            h_out = true;
            h_Object.SetActive(true);
            h_Object.GetComponent<HookHelper_TopDown>().FireHook(this.transform.position, firingDirection);
        }
    }

    // External objects can tell the hook controller to disconnect hooks
    public void DisconnectHook()
    {
        ChangeHookConnectedState(false);
    }

    private void HookHitGround(HookSide hookSide)
    {
        if (hookSide == HookSide.Right)
        {
            ChangeHookConnectedState(true);
        }
    }

    private void HookHitHazard(HookSide hookSide)
    {
        if (h_out || IsConnected())
            DisconnectHook();
    }

    private void ChangeHookConnectedState(bool toState)
    {
        // Disable/Enable the joint and set its distance
        h_Joint.enabled = toState;
        if (toState)
        {
            h_out = true;
            h_onGround = true;
            float dist = Vector2.Distance(this.transform.position, h_Object.transform.position);
            h_Joint.distance = dist;
        }
        else
        {
            h_out = false;
            h_onGround = false;
            DisconnectParticleEffect();
            h_Object.SetActive(false);
        }
    }

    public void ReelHook(float inputReelValue)
    {
        // Reel if the hook is on ground
        if (h_onGround && inputReelValue >= inputReelMinimum)
        {
            // Calculate amount to reel
            reelToApply += reelPerSec;
            if (reelToApply >= maxReelSpeed)
                reelToApply = maxReelSpeed;
            reelToApply *= inputReelValue;
        }
        else
        {
            reelToApply = 0;
        }
    }

    #region Visuals
    private void DrawHook()
    {
        if (dashPossible)
            h_Line.startColor = h_Line.endColor = dashingPossibleColor;
        else
            h_Line.startColor = h_Line.endColor = regularColor;

        h_Line.SetPosition(0, this.transform.position);
        h_Line.SetPosition(1, h_Object.transform.position);
    }

    private void DisconnectParticleEffect()
    {
        // Get info for changing shape
        Vector3 hereToHook = h_Object.transform.position - this.transform.position;
        float rotation = Vector3.SignedAngle(Vector3.up, hereToHook, Vector3.forward);
        float lineLength = Vector3.Distance(this.transform.position, h_Object.transform.position);

        // Change number of particles emitted based on length
        var em = line_ps.emission;
        em.SetBursts(
             new ParticleSystem.Burst[] {
                  new ParticleSystem.Burst (0.0f, lineLength * 2.5f)
             });

        // Change velocity
        var vel = line_ps.velocityOverLifetime;
        vel.x = new ParticleSystem.MinMaxCurve(-5f, 5f);
        vel.y = new ParticleSystem.MinMaxCurve(-5f, 5f);

        // Change looping
        var main = line_ps.main;
        main.loop = false;

        // Change shape
        var sh = line_ps.shape;
        sh.position = Vector3.zero + hereToHook.normalized * (lineLength / 2);
        sh.rotation = new Vector3(0, 0, rotation);
        sh.scale = new Vector3(0, lineLength, 0);

        line_ps.Play(false);
    }

    private void StartContinuousPartileEffect()
    {

        // Change looping
        var main = line_ps.main;
        main.loop = true;

        // Disable bursts
        var em = line_ps.emission;
        em.SetBursts(
             new ParticleSystem.Burst[0] { });

        // Change velocity
        var vel = line_ps.velocityOverLifetime;
        vel.x = new ParticleSystem.MinMaxCurve(-2f, 2f);
        vel.y = new ParticleSystem.MinMaxCurve(-2f, 2f);

        line_ps.Play(false);
        StartCoroutine(ContinuousParticleEffectMaintenance());
    }

    private IEnumerator ContinuousParticleEffectMaintenance()
    {
        while (line_ps.isPlaying)
        {
            // Get info for changing shape
            Vector3 hereToHook = h_Object.transform.position - this.transform.position;
            float rotation = Vector3.SignedAngle(Vector3.up, hereToHook, Vector3.forward);
            float lineLength = Vector3.Distance(this.transform.position, h_Object.transform.position);

            // Change number of particles emitted based on length
            var emission = line_ps.emission;
            emission.rateOverTime = lineLength * 3.5f;

            // Change shape
            var shape = line_ps.shape;
            shape.position = Vector3.zero + hereToHook.normalized * (lineLength / 2);
            shape.rotation = new Vector3(0, 0, rotation);
            shape.scale = new Vector3(0, lineLength, 0);

            // Change color
            var main = line_ps.main;
            if (dashPossible)
                main.startColor = dashingPossibleColor;
            else
                main.startColor = regularColor;

            yield return null;
        }
    }
    #endregion

    public void SetLineContainer(GameObject lineContainer)
    {
        h_LineContainer = lineContainer;
    }

    public bool IsConnected()
    {
        return (h_out && h_onGround);
    }
}


