﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class HookHelper : MonoBehaviour
{
    public static event Action<HookSide> OnHookHitGround;
    public static event Action<HookSide> OnHookHitHazard;

    public float firingSpeed;
    public HookSide hookSide;
    public Rigidbody2D rb;

    public float timeToTravel;
    public LayerMask interactable;
    CircleCollider2D mainCollider;

    private bool firing = false;
    private bool collisionPriority = false;
    private Vector2 nextPosition;
    private Vector2 firingDirection = new Vector2();
    private Transform origParent;

    private HookController hookController;

    private void Awake()
    {
        origParent = this.transform.parent;
        hookController = origParent.GetComponentInChildren<HookController>();
    }

    private void Start()
    {
        origParent = this.transform.parent;
        mainCollider = this.GetComponent<CircleCollider2D>();
    }

    private void OnEnable()
    {
        if(origParent != null)
        {
            this.transform.SetParent(origParent);
        }
    }

    private void OnDisable()
    {
        firing = false;
        rb.freezeRotation = false;
        collisionPriority = false;
    }

    private void Update()
    {    
        if (firing && !hookController.HookOnGround)
        {
            RaycastHit2D rayHit = Physics2D.CircleCast(this.transform.position, 0.11f, (Vector3)firingDirection.normalized, firingSpeed * Time.deltaTime, interactable);

            if (rayHit.collider == null)
            {
                nextPosition = this.transform.position;
                nextPosition += firingDirection.normalized * firingSpeed * Time.deltaTime;
            }
            else
            {
                
                nextPosition = rayHit.point;
            }
            rb.MovePosition(nextPosition);
        }

        if (hookController.HookOnGround && rb.bodyType != RigidbodyType2D.Kinematic)
        {
            // Catches the edge cases in which the rigidbody does not update (for some unknown reason)
            rb.bodyType = RigidbodyType2D.Kinematic;
        }
    }

    public void FireHook(Vector2 startingPosition, Vector2 directionToFire)
    {
        firingDirection = directionToFire.normalized;
        this.transform.position = startingPosition;
        firing = true;
        this.transform.eulerAngles = new Vector3(0f, 0f, Vector2.SignedAngle(Vector2.up, firingDirection));
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.freezeRotation = true;

        int randomNum = UnityEngine.Random.Range(1, 4);
        AudioManager.instance.PlaySound("HookFire" + randomNum);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CollisionWithPriority();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CollisionWithPriority();
    }

    //In the case where multiple colliders were hit in the same frame, hazards sometimes were hit first
    //Now looking at all colliders in order to make sure ground always gets priority
    private void CollisionWithPriority()
    {
        Collider2D[] allColliders = Physics2D.OverlapCircleAll(this.transform.position, mainCollider.radius, interactable);

        foreach (Collider2D curCollider in allColliders)
        {
            if (curCollider.gameObject.layer == LayerMask.NameToLayer("Ground") && !hookController.HookOnGround && !collisionPriority)
            {
                //Debug.Log("Hook Hit Ground");
                firing = false;
                
                this.transform.SetParent(curCollider.transform);
                rb.bodyType = RigidbodyType2D.Kinematic;    // Fixes the hook in place
                OnHookHitGround?.Invoke(hookSide);
                break;
            }
            else if (curCollider.gameObject.layer == LayerMask.NameToLayer("Hazard") && !hookController.HookOnGround && !collisionPriority)
            {
                //Debug.Log("Hook Hit Hazard trig");
                firing = false;
                OnHookHitHazard?.Invoke(hookSide);
                break;
            }
            else if (curCollider.gameObject.layer == LayerMask.NameToLayer("CameraEdge") && !hookController.HookOnGround && !collisionPriority)
            {
                //Debug.Log("Hook Hit Camera Edge");
                firing = false;
                OnHookHitHazard?.Invoke(hookSide);
                break;
            }
        }
    }

    private GUIStyle bigFont = new GUIStyle();

    private void OnGUI()
    {
        if (DebugOptions.debugText)
        {
            bigFont.fontSize = 20;

            GUI.Label(new Rect(400, 200, 1000, 1000),
                "\nPosition:     " + transform.position
                , bigFont);

        }
    }


}
