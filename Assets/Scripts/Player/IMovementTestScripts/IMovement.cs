using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    Vector2 VelocityToAddThisFrame { get; }
    ForceMode2D ForceMode { get; }
}
