using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerGroundMovement : MonoBehaviour, IMovement
{
    //TODO: Implement vector scaling based on current RB.velocity in this class
    //TODO: Fix 'sliding' bug caused when switching directions (horizontalInput is not zeroing when switching from +1 to -1, vice versa)
    
    public Vector2 VelocityToAddThisFrame => currentGroundVelocity;

    public ForceMode2D ForceMode => ForceMode2D.Force;

    private Vector2 currentGroundVelocity;
    private Vector2 velocityLastFrame;

    private float horizontalInput;

    private float currentHorizontalGroundSpeed;
    private const float maxHorizontalGroundSpeed = 5.0f;

    private PlayerControls playerControls;
    private GUIStyle bigFont = new GUIStyle();

    private void Awake() 
    {
        playerControls = new PlayerControls();    
    }

    // Start is called before the first frame update
    void Start()
    {
        currentGroundVelocity = Vector2.zero;
        
        horizontalInput = 0f;

        bigFont.normal.textColor = Color.white;
        bigFont.fontSize = 15;
    }

    private void OnEnable() 
    {
        playerControls.OneHook.HoriztonalAxis.performed += InitiateHorizontalMovement;
        //playerControls.OneHook.HoriztonalAxis.canceled += DisableHorizontalMovement;
        playerControls.OneHook.HoriztonalAxis.Enable();  
    }

    private void OnDisable() 
    {
        playerControls.OneHook.HoriztonalAxis.performed -= InitiateHorizontalMovement;
        //playerControls.OneHook.HoriztonalAxis.canceled -= DisableHorizontalMovement;
        playerControls.OneHook.HoriztonalAxis.Disable();    
    }

    private void InitiateHorizontalMovement(InputAction.CallbackContext obj)
    {
        horizontalInput = obj.ReadValue<float>();
        //currentGroundVelocity = new Vector2(maxHorizontalGroundSpeed * horizontalInput, 0f);
        //Debug.Log($"Horizontal Input: {horizontalInput}");
    }
    
    private void DisableHorizontalMovement(InputAction.CallbackContext obj)
    {
        horizontalInput = 0;
        Debug.Log("INPUT CANCELLED!");
    }

    private float currentSpeedDifference;
    private float UpdateCurrentGroundSpeed(float input)
    {
        // TODO: Add Check against Deadzone value
        // input = input < deadZoneValue ? 0 : input;
        if ( input == 0 )
        {
            // TODO: Add a const deceleration amount for this LERP
            currentHorizontalGroundSpeed = Mathf.Lerp(currentHorizontalGroundSpeed, 0, 0.25f) * Time.deltaTime;
            return currentHorizontalGroundSpeed;
        }
        
        float inputSign = Mathf.Sign(input);
        if ( Mathf.Sign(currentHorizontalGroundSpeed) != inputSign )
            currentHorizontalGroundSpeed *= -1;

        
        currentSpeedDifference = maxHorizontalGroundSpeed - Mathf.Abs(currentHorizontalGroundSpeed);
        //TODO: Add a const threshold value here
        if ( currentSpeedDifference > 0.01f )
        {
            // TODO: Add variable for lerping amount
            currentHorizontalGroundSpeed += Mathf.Lerp( currentHorizontalGroundSpeed, inputSign * maxHorizontalGroundSpeed, 0.3f ) * Time.deltaTime;
            currentHorizontalGroundSpeed += inputSign * 5f * Time.deltaTime;
        } else
        {
            currentHorizontalGroundSpeed = inputSign * maxHorizontalGroundSpeed;
        }
        return currentHorizontalGroundSpeed;
    }
    private void Update()
    {
        currentHorizontalGroundSpeed = UpdateCurrentGroundSpeed(horizontalInput);
        
        //TODO: Add threshold varialbe for zeroing ground speed
        if ( Mathf.Abs(currentHorizontalGroundSpeed) < 0.001f )
            currentHorizontalGroundSpeed = 0;
        
        currentGroundVelocity = new Vector2(currentHorizontalGroundSpeed, 0);
    }

    private void OnGUI() 
    {
        GUI.Label(new Rect(100, 100, 200, 200),
            $"Horizontal Input: {horizontalInput}\n" +
            $"Current Speed:    {currentHorizontalGroundSpeed}\n",
            bigFont
        );
    }
}
