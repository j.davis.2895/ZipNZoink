using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerJump : MonoBehaviour, IMovement
{
    public Vector2 VelocityToAddThisFrame => jumpVelocity;

    public ForceMode2D ForceMode => ForceMode2D.Impulse;

    private Vector2 jumpVelocity;

    [SerializeField]
    private float jumpAmount = 5f;

    private Coroutine jumpInstance;
    private PlayerControls playerControls;

    private void Awake()
    {
        playerControls = new PlayerControls();    
    }

    private void OnEnable() 
    {
        playerControls.OneHook.Jump.performed += OnJump;
        playerControls.OneHook.Jump.Enable();    
    }

    private void OnDisable() 
    {
        playerControls.OneHook.Jump.performed -= OnJump;
        playerControls.OneHook.Jump.Disable();       
    }

    private void OnJump(InputAction.CallbackContext obj)
    {
        if ( jumpInstance != null ) return;
        
        jumpVelocity = new Vector2( 0, jumpAmount );
        jumpInstance = StartCoroutine(nameof(ResetJumpVelocity));
    }

    private IEnumerator ResetJumpVelocity()
    {
        yield return null;
        jumpVelocity = Vector2.zero;
        jumpInstance = null;
    }
}
