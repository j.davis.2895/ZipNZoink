using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerMovementHandler : MonoBehaviour
{
    //TODO: Move vector scaling and zeroing logic to individual IMovement classes
    //TODO: Test how forces currently interact with the player movement handled here
    
    private List<IMovement> movementHandlers = new List<IMovement>();
    
    [Tooltip("The maximum speed the character should approach in m/s")]
    [SerializeField]
    private float maxVeloctiyMagnitude;
    private Queue<Vector2> velocityToResolveThisFrame = new Queue<Vector2>();
    
    private Rigidbody2D playerRB;

    private void Awake() 
    {
        playerRB = this.GetComponent<Rigidbody2D>();
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        movementHandlers = this.GetComponents<IMovement>().ToList();

        bigFont.normal.textColor = Color.red;
        bigFont.fontSize = 15;
    }

    // Update is called once per frame
    private void Update()
    {
        foreach ( IMovement handler in movementHandlers )
        {
            Vector2 velocityFromHandler = handler.VelocityToAddThisFrame;

            if ( velocityFromHandler != Vector2.zero )
            {
                velocityToResolveThisFrame.Enqueue(velocityFromHandler);
            }
        }
    }

    private void FixedUpdate() 
    {
        //Vector2 velocityThisFrame = Vector2.zero;
        //playerRB.velocity = Vector2.zero;

        if ( velocityToResolveThisFrame.Count == 0 )
        {
            float currentXVelocity = playerRB.velocity.x;
            currentXVelocity = Mathf.Lerp(currentXVelocity, 0, 0.15f);

            playerRB.velocity = new Vector2(currentXVelocity, playerRB.velocity.y);
            return;
        }

        while ( velocityToResolveThisFrame.Count > 0 )
        {
            Vector2 velocityToAdd = velocityToResolveThisFrame.Dequeue();

            float xVelocityScaled = ( maxVeloctiyMagnitude - Mathf.Abs(playerRB.velocity.x) ) / maxVeloctiyMagnitude;
            float yVelocityScaled = ( maxVeloctiyMagnitude - Mathf.Abs(playerRB.velocity.y) ) / maxVeloctiyMagnitude;

            xVelocityScaled = Mathf.Clamp(xVelocityScaled, 0, maxVeloctiyMagnitude);
            yVelocityScaled = Mathf.Clamp(yVelocityScaled, 0, maxVeloctiyMagnitude);
            
            Vector2 scaledVelocityToAdd = new Vector2(velocityToAdd.x * xVelocityScaled, velocityToAdd.y * yVelocityScaled);

            playerRB.velocity += scaledVelocityToAdd;
        }
    }

    private GUIStyle bigFont = new GUIStyle();
    private void OnGUI() 
    {
        GUI.Label(new Rect(100, 300, 200, 200),
            $"Current Velocity: {playerRB.velocity.sqrMagnitude}" +
            $"\nVelocity should zero out? {playerRB.velocity.sqrMagnitude < 0.1f}",
            bigFont
        );

    }

}
