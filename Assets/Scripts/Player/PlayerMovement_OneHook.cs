﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerMovement_OneHook : MonoBehaviour
{
    #region Variables

    public static UnityEvent DetachHook;

    [Header("Hook Setup")]
    public GameObject hookObject;     // The hook head for the right hook.
    private HookController _hookcontroller;
    public HookController HookController
    {
        get { return _hookcontroller; }
        private set { }
    }
    public HookControllerCommonSetup commonHookData;    // Common data for hook setup
    //private bool hookR_connected = false;

    /*** MOVEMENT DATA ***/
    private Rigidbody2D rb;
    [SerializeField] private CircleCollider2D bottomCollider = new CircleCollider2D();
    [SerializeField] private LayerMask groundLayer = new LayerMask();

    [Header("Ground Movement")]
    public float accelerateValueHorizontal = 30;
    public float decelerateValueHorizontal = 50;
    public float maxGroundSpeedViaInput = 7;
    public float movementCancelHelper = 4;
    public bool groundAirBraking = false;

    [Header("Jump Movement")]
    public float jumpForce = 15.2f;
    public float jumpCutoff = 4;
    public float jumpCancelGravityMult = 1.5f;
    //Jump buffer adds a buffer when the player presses the jump button, which can help if the press the button "too early" while in the air
    public float jumpBufferTime = 0.1f;
    private bool jumpBuffered = false;
    private bool jumpCancelling = false;
    //hangTime is used to allow the player a little bit of time to jump after walking off of a ledge 
    public float hangTime = 0.2f;
    //hangTimer does not need to be public, currently is public for testing
    public float hangTimer;

    [Header("Air Movement")]
    public float airAccelerateValue = 10;
    public float airDecelerateValue = 10;
    public float maxHorizontalAirSpeed = 7;
    public float dashForceMult = 38.3f;
    public float wallJumpAirDisableTime = 0.2f;
    private bool allowAirMovement = true;
    private bool airBrakeQueued = false;
    private bool allowAirBraking = true;
    public float airBrakeCooldownTime = 1f;
    public float airBrakeJumpForceMult = 0.2f;
    private float airBrakeAirDisableTime = 0.02f;
    public float airBrakeModifierTime = 1f;
    public float airBrakeGravityMult = 0.5f;
    private bool isAirBraking = false;

    [Header("Hook Movement")]
    public float horHookMoveMult = 30;
    public float hookJumpForceMult = 3;
    public float hookJumpHorizMult = 0.7f;
    public float superHookJumpForceMult = 10;
    public float hookDashMoveMagnitude = 20;
    public float dashBufferTime = 0.1f;
    public float dashCooldownTime = 1f;
    private bool dashingAllowed = true;
    private bool dashQueued = false;
    private bool dashBuffered = false;

    [Header("Wall Movement")]
    //The speed the player slides down a wall
    //Moving entirely by gravity with y set to 0
    //NOTE: might, if using values lower than 0, have to clamp to that value
    public float wallSlideSpeed = 0.0f;
    //The speed the player jumps off the wall in the x direction
    public float wallStepOffSpeed = 8.5f;
    public float wallJumpOffForce = 8.5f;
    public float wallJumpBufferTime;
    private bool wallJumpBuffered = false;
    private bool isWallSliding = false;
    public bool IsWallSliding
    {
        get { return isWallSliding; }
        protected set { }
    }
    private bool hasWallBehind = false;
    private bool isOnWall = false;
    private bool wallJumpQueued;
    private int wallSide = 0;
    public float curHorSpeed; // Use inspector in debug mode to see this
    public Vector2 watchthis;
    private int wallHangState = 0;
    public float wallHangTime = 0.1f;
    private float wallHangSavedInput = 0f;

    [Header("Movement Helpers")]
    private float hookSwingToApply;
    private bool jumpQueued = false;
    private bool isAirJumping;
    private HoriDirection directionFacing;    // -1 is left, 1 is right
    private HoriDirection directionWhenJumpStarted;
    private float rbDefaultGravityScale;
    private static float deadZoneValue = 0.1f;
    //TODO: Need to investigate how this works with air movement

    [Header("Input Containers")]
    private float curHorInput = 0;
    private float curVerInput = 0;  // One Hook Variant
    private float reelRightHook = 0;

    [Header("Visual Data")] // Most of this should eventually just be handles by an animator
    public GameObject lineRenderContainer;
    private SpriteRenderer playerSprite;
    public Animator charAnimator;
    public VisualAimAssist aimAssist;
    public ParticleSystem dashParticleSystem;
    public AirBrakeVisualHelper airBrakeVisuals;

    // NEW INPUT SYSTEM
    PlayerControls controls;
    #endregion

    #region Initialization Methods

    private void Awake()
    {
        if (DetachHook == null) DetachHook = new UnityEvent();

        _hookcontroller = this.gameObject.GetComponent<HookController>();
        _hookcontroller.SetLineContainer(lineRenderContainer);
        _hookcontroller.SetupHook(hookObject, commonHookData);

        rb = GetComponent<Rigidbody2D>();
        rbDefaultGravityScale = rb.gravityScale;

        playerSprite = this.GetComponentInChildren<SpriteRenderer>();

        // New input system
        controls = new PlayerControls();
    }

    private void OnEnable()
    {
        HookHelper.OnHookHitGround += HookHitGround;

        controls.OneHook.HoriztonalAxis.performed += HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed += HandleVerticalaxis;
        controls.OneHook.Jump.performed += HandleJump;
        controls.OneHook.Jump.canceled += HandleJump;
        controls.OneHook.Fire.performed += HandleFire;
        controls.OneHook.Reel.performed += HandleReel;
        controls.OneHook.HookDash.performed += HandleHookDash;
        controls.OneHook.AirBrake.performed += HandleAirBrake;
        controls.OneHook.AirBrake.canceled += HandleAirBrake;

        controls.OneHook.HoriztonalAxis.Enable();
        controls.OneHook.VerticalAxis.Enable();
        controls.OneHook.Jump.Enable();
        controls.OneHook.Fire.Enable();
        controls.OneHook.Reel.Enable();
        controls.OneHook.HookDash.Enable();
        controls.OneHook.AirBrake.Enable();
    }

    private void OnDisable()
    {
        HookHelper.OnHookHitGround -= HookHitGround;

        controls.OneHook.HoriztonalAxis.performed -= HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed -= HandleVerticalaxis;
        controls.OneHook.Jump.performed -= HandleJump;
        controls.OneHook.Jump.canceled -= HandleJump;
        controls.OneHook.Fire.performed -= HandleFire;
        controls.OneHook.Reel.performed -= HandleReel;
        controls.OneHook.HookDash.performed -= HandleHookDash;
        controls.OneHook.AirBrake.performed -= HandleAirBrake;
        controls.OneHook.AirBrake.canceled -= HandleAirBrake;

        controls.OneHook.HoriztonalAxis.Disable();
        controls.OneHook.VerticalAxis.Disable();
        controls.OneHook.Jump.Disable();
        controls.OneHook.Fire.Disable();
        controls.OneHook.Reel.Disable();
        controls.OneHook.HookDash.Disable();
        controls.OneHook.AirBrake.Disable();
    }

    private void Start()
    {
        curHorSpeed = 0.0f;
    }
    #endregion

    #region Input Handlers
    private void HandleHorizontalAxis(InputAction.CallbackContext obj)
    {
        curHorInput = obj.ReadValue<float>();

        if (Mathf.Abs(curHorInput) < deadZoneValue)
        {
            curHorInput = 0;
        }
    }

    private void HandleVerticalaxis(InputAction.CallbackContext obj)
    {
        curVerInput = obj.ReadValue<float>();
    }

    private void HandleReel(InputAction.CallbackContext obj)
    {
        reelRightHook = obj.ReadValue<float>();
    }

    private void HandleFire(InputAction.CallbackContext obj)
    {
        if (aimAssist.CurrentAimingData.validTarget)
        {
            Vector2 firingDirection = aimAssist.CurrentAimingData.targetPosition - this.transform.position;
            _hookcontroller.FireHook(firingDirection.normalized);
        }
        else
        {
            _hookcontroller.FireHook(SnapOctDirection(curHorInput, curVerInput));
        }
    }

    private void HandleHookDash(InputAction.CallbackContext obj)
    {
        if (dashingAllowed && !IsGrounded())
        {
            StartCoroutine(DashCooldown(dashCooldownTime));
            if (IsHooked())
            {
                StartCoroutine(DashBufferTimer(dashBufferTime));
                if (rb.velocity.magnitude >= hookDashMoveMagnitude || rb.velocity.magnitude >= 10.0f)
                {
                    //No point dashing if player is already moving faster than the dash
                    //Also don't want to dash if already moving past a certain speed
                    return;
                }

                Vector2 hookPosition = new Vector2(hookObject.transform.position.x, hookObject.transform.position.y);
                if (transform.position.y >= (hookPosition.y - 0.1f))
                {
                    //Don't want to dash if above the hooked position
                    return;
                }

                dashQueued = true;
            }

        }
    }

    private void HandleAirBrake(InputAction.CallbackContext obj)
    {
        if (IsGrounded())
        {
            if (!obj.canceled)
                groundAirBraking = true;
            else
                groundAirBraking = false;
        }
        else
        {
            groundAirBraking = false;
            if (allowAirBraking && !isOnWall)
            {
                StartCoroutine(AirBrakeCooldown(airBrakeCooldownTime));
                airBrakeQueued = true;
            }
        }
    }

    private void HandleJump(InputAction.CallbackContext obj)
    {
        jumpCancelling = obj.canceled;

        if (IsHooked())
        {
            if (!jumpQueued)
            {
                jumpQueued = obj.performed;
            }
        }
        else if (isWallSliding)
        {
            if (!wallJumpQueued)
            {
                wallJumpQueued = obj.performed;
            }
        }
        else
        {
            if (obj.canceled && !isAirJumping)
            {
                if (rb.velocity.y > jumpCutoff)
                {
                    rb.gravityScale = rbDefaultGravityScale * jumpCancelGravityMult; // need to put in a check, if the player is falling faster than a 'max fall speed', change gravity scale back to default
                }
            }

        }

        // Hang time
        if (hangTimer > 0.0f)
        {
            if (!jumpQueued)
            {
                jumpQueued = obj.performed;
            }
        }

        // Jump buffering
        if (obj.performed && !IsHooked() && !IsGrounded())
        {
            //Debug.Log("Jump Buffered");
            StartCoroutine(JumpBufferTimer(jumpBufferTime));
            StartCoroutine(WallJumpBufferTimer(wallJumpBufferTime));
        }
    }
    #endregion

    #region Buffers
    public IEnumerator JumpBufferTimer(float bufferTime)
    {
        jumpBuffered = true;
        yield return new WaitForSeconds(bufferTime);
        jumpBuffered = false;
    }

    public IEnumerator WallJumpBufferTimer(float bufferTime)
    {
        wallJumpBuffered = true;
        yield return new WaitForSeconds(bufferTime);
        wallJumpBuffered = false;
    }

    public IEnumerator DisableAirMovement(float disableTime)
    {
        allowAirMovement = false;
        yield return new WaitForSeconds(disableTime);
        allowAirMovement = true;
    }

    public IEnumerator DashCooldown(float disableTime)
    {
        dashingAllowed = false;
        yield return new WaitForSeconds(disableTime);
        dashingAllowed = true;
    }

    public IEnumerator AirBrakeCooldown(float disableTime)
    {
        allowAirBraking = false;
        yield return new WaitForSeconds(disableTime);
        allowAirBraking = true;
    }

    public IEnumerator DashBufferTimer(float bufferTime)
    {
        dashBuffered = true;
        yield return new WaitForSeconds(bufferTime);
        dashBuffered = false;
    }

    public IEnumerator WallHangTimer(float hangTime)
    {
        yield return new WaitForSeconds(hangTime);
        // Only change to state 2 if state is 1 by the end of the timer
        if (wallHangState == 1)
        {
            wallHangState = 2;
        }
    }

    public IEnumerator AirBrakeModifier(float modifierTime)
    {
        float startTime = Time.time;
        rb.gravityScale = rbDefaultGravityScale * airBrakeGravityMult;
        isAirBraking = true;
        float orginAccel, originDecel;
        orginAccel = airAccelerateValue;
        originDecel = airDecelerateValue;
        airAccelerateValue = orginAccel * 0.6f;
        airDecelerateValue = originDecel * 2f;

        airBrakeVisuals.PlayVisuals(modifierTime);

        while (Time.time < startTime + modifierTime)
        {
            yield return null;
            if (IsGrounded() || IsHooked() || isOnWall)
            {
                break;
            }
            else if (Time.time > startTime + (modifierTime/2))
            {
                // Smooth transition to default gravity
                airAccelerateValue = orginAccel;
                airDecelerateValue = originDecel;
                float percent = (Time.time - (startTime + (modifierTime/2))) / (modifierTime/2);
                rb.gravityScale = rbDefaultGravityScale * Mathf.Pow(airBrakeGravityMult, 1 - percent);
            }
        }
        airAccelerateValue = orginAccel;
        airDecelerateValue = originDecel;

        airBrakeVisuals.StopVisuals();

        rb.gravityScale = rbDefaultGravityScale;
        isAirBraking = false;
    }
    #endregion

    private void Update()
    {
        // NOTE: This should be the only section that modifies "directionFacing"
        if (curHorInput > 0)
        {
            directionFacing = HoriDirection.Right;
            float localScaleX = 1f;
            //localScaleX = allowAirMovement ? 1f : -1f;
            localScaleX = hasWallBehind ? -1f : 1f;
            playerSprite.transform.localScale = new Vector3(localScaleX, 1, 1);
        }
        else if (curHorInput < 0)
        {
            directionFacing = HoriDirection.Left;
            float localScaleX = -1f;
            //localScaleX = allowAirMovement ? -1f : 1f;
            localScaleX = hasWallBehind ? 1f : -1f;
            playerSprite.transform.localScale = new Vector3(localScaleX, 1, 1);
        }

        hookSwingToApply = 0;

        if (IsGrounded())
        {
            // Set defaults
            rb.gravityScale = rbDefaultGravityScale;
            isAirJumping = false;
            isWallSliding = false;
            wallJumpBuffered = false;
            isOnWall = false;
            hasWallBehind = false;
            hangTimer = hangTime;

            // Animation
            charAnimator.SetBool("IsGrounded", true);
        }
        else
        {
            hangTimer -= Time.deltaTime;

            // Animation
            charAnimator.SetBool("IsGrounded", false);

            if (IsHooked())
            {
                // Set defaults
                rb.gravityScale = rbDefaultGravityScale;

                curHorSpeed = rb.velocity.x;
                hookSwingToApply = curHorInput * horHookMoveMult;

                isWallSliding = false;
                isAirJumping = false;
                wallJumpBuffered = false;
                isOnWall = false;
                hasWallBehind = false;

                // Animation
                charAnimator.SetBool("IsHooked", true);
            } 
            else
            {
                // Animation
                charAnimator.SetBool("IsHooked", false);

                // Check for pressing against wall or if still on wall
                if (curHorInput != 0 || isOnWall)
                {
                    float distance = 0.03f;
                    Vector3 castOrigin = bottomCollider.bounds.center;
                    Vector2 castSize = new Vector2(bottomCollider.radius * 2f, bottomCollider.radius * 0.6f);
                    RaycastHit2D raycastHit = Physics2D.BoxCast(castOrigin, castSize, 0f, new Vector2((int)directionFacing, 0f), distance, groundLayer);
                    RaycastHit2D raycastHitBehind = Physics2D.BoxCast(castOrigin, castSize, 0f, new Vector2((int)directionFacing * -1, 0f), distance, groundLayer);
                    
                    isOnWall = raycastHit.collider != null;
                    hasWallBehind = raycastHitBehind.collider != null;
                    if (isOnWall || hasWallBehind)
                    {
                        wallSide = isOnWall ? (int)directionFacing : (int)directionFacing * -1;
                        if (wallJumpBuffered)
                        {
                            wallJumpQueued = true;
                        }
                    }
                }
            }
        }


        // Hang time
        if (hangTimer > 0.0f)
        {
            if (!jumpQueued)
            {
                //jumpQueued = jumpInput;
            }
        }

        // Jump cancelling
        if (!IsHooked() && !isWallSliding && jumpCancelling && !isAirBraking)
        {
            rb.gravityScale = rbDefaultGravityScale * jumpCancelGravityMult; // need to put in a check, if the player is falling faster than a 'max fall speed', change gravity scale back to default
        }

        // Check for buffered jump
        if (jumpBuffered && !_hookcontroller.IsConnected() && IsGrounded())
        {
            jumpQueued = true;
        }

        if (dashBuffered && _hookcontroller.DashPossible)
        {
            dashQueued = true;
        }

        ControlHooks();
        watchthis = rb.velocity;
    }

    private void LateUpdate()
    {
        //Animation
        charAnimator.SetFloat("AbsXVelocity", Mathf.Abs(rb.velocity.x));
        charAnimator.SetFloat("RelHorizontalInput", Mathf.Sign(rb.velocity.x) * curHorInput);
        charAnimator.SetFloat("RelYVelocity", rb.velocity.y);
        charAnimator.SetBool("IsOnWall", (isOnWall || hasWallBehind));
    }

    public bool IsGrounded()
    {
        float extraHeight = 0.03f;
        Vector3 castOrigin = bottomCollider.bounds.center;
        Vector2 castSize = new Vector2(bottomCollider.radius * 2, bottomCollider.radius * 2);
        RaycastHit2D raycastHit = Physics2D.BoxCast(castOrigin, castSize, 0f, Vector2.down, extraHeight, groundLayer);

        //One Way Platform Conditional: Make sure the player is ABOVE the platform before IsGrounded() is true
        if (raycastHit.collider != null && raycastHit.collider.TryGetComponent(out PlatformEffector2D platEffector))
        {
            if (platEffector.useOneWay && rb.velocity.y > 0.1f || this.transform.position.y - 0.5f < raycastHit.point.y)
            {
                return false;
            }
        }

        return raycastHit.collider != null;
    }

    private void ControlHooks()
    {
        //TODO: Stephen: I think we should check the hook state using public bools from Hook Controller, rather than a local variable that gets toggled
        //hookR_connected = hookR_controller.HookOnGround;

        _hookcontroller.ReelHook(reelRightHook);
    }

    private void FixedUpdate()
    {
        if (!IsGrounded())
        {
            if (IsHooked())
            {
                Vector2 forceToApply = new Vector2(hookSwingToApply * Time.fixedDeltaTime, 0);
                rb.AddForce(forceToApply);
            }
            else if (isWallSliding)
            {
                //HandleWallSliding();
            }
            else
            {
                if (allowAirMovement)
                {
                    ApplyHorizontalAirMovement();
                }
                else
                {
                    curHorSpeed = rb.velocity.x;
                }
            }
        }
        else
        {
            ApplyGroundMovement();
        }

        HandleWallSliding();
        DashLogic();
        JumpLogic();
        AirBrakeLogic();
    }

    private void HandleWallSliding()
    {
        //check for wall in the direction the player is currently moving
        //if player collides with that wall, set wall sliding to true
        //To make sure the player doesn't get stuck on small blocks
        if (rb.velocity.y < 2.0f && isOnWall && curHorInput != 0)
        {
            isWallSliding = true;
            //Using this to correct for oddities with adjusting the gravity scale
            rb.gravityScale = rbDefaultGravityScale;
        }
        else
        {
            isWallSliding = false;
        }

        if (isWallSliding)
        {
            rb.velocity = new Vector2(0, wallSlideSpeed);
            curHorSpeed = 0;
            wallHangState = 0;
        }

        // Slide/Step off the wall
        if (hasWallBehind && curHorInput * wallSide < 0)
        {
            switch (wallHangState)
            {
                case 0:
                    wallHangState = 1;
                    StartCoroutine(WallHangTimer(wallHangTime));
                    break;
                case 1:
                    rb.velocity = new Vector2(0, wallSlideSpeed);
                    curHorSpeed = 0;
                    break;
                case 2:
                    curHorSpeed = -wallSide * wallStepOffSpeed;
                    hasWallBehind = false;
                    //isWallSliding = false;
                    rb.gravityScale = rbDefaultGravityScale;
                    break;
            }
        }

        // Cancel Step off during hang time
        if (hasWallBehind && curHorInput * wallSide > 0 && wallHangState == 1)
        {
            wallHangState = 0;
        }
    }

    #region FixedUpdate Movement Methods

    private void ApplyHorizontalAirMovement()
    {
        curHorSpeed += airAccelerateValue * curHorInput * Time.fixedDeltaTime;

        if (Mathf.Abs(curHorSpeed) > maxHorizontalAirSpeed)
        {
            if (curHorSpeed > 0)
            {
                curHorSpeed -= airDecelerateValue * Time.fixedDeltaTime;
                if (curHorInput < 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += airAccelerateValue * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
            }
            else if (curHorSpeed < 0)
            {
                curHorSpeed += airDecelerateValue * Time.fixedDeltaTime;
                if (curHorInput > 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += airAccelerateValue * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
            }
        }
        rb.velocity = new Vector2(curHorSpeed, rb.velocity.y);
    }

    private void ApplyGroundMovement()
    {
        float tempInputHolder = 0f;

        if (groundAirBraking)
        {
            tempInputHolder = curHorInput;
            curHorInput = 0;
        }

        // The following functions modify the 'curHorSpeed' variable
        if (curHorInput == 0)
        {
            HandleGroundAcceleration();
        }
        else
        {
            HandleGroundDeceleration();
        }

        // Once the 'curHorSpeed' variable is set above, it is applied here
        rb.velocity = new Vector2(curHorSpeed, rb.velocity.y);

        if (groundAirBraking)
        {
            // This is to allow the continuation of horizontal movement if the player kept the same movement key held down during the ground-air braking process
            curHorInput = tempInputHolder;
        }
    }

    private void HandleGroundAcceleration()
    {
        if (curHorSpeed > 0)
        {
            curHorSpeed -= decelerateValueHorizontal * Time.fixedDeltaTime;
            if (curHorSpeed < 0)
            {
                curHorSpeed = 0;
            }
        }
        else if (curHorSpeed < 0)
        {
            curHorSpeed += decelerateValueHorizontal * Time.fixedDeltaTime;
            if (curHorSpeed > 0)
            {
                curHorSpeed = 0;
            }
        }
    }

    private void HandleGroundDeceleration()
    {
        if (Mathf.Abs(curHorSpeed) > maxGroundSpeedViaInput)
        {
            if (curHorSpeed > 0)
            {
                curHorSpeed -= decelerateValueHorizontal * Time.fixedDeltaTime;
                if (curHorInput < 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += accelerateValueHorizontal * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
            }
            else if (curHorSpeed < 0)
            {
                curHorSpeed += decelerateValueHorizontal * Time.fixedDeltaTime;
                if (curHorInput > 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += accelerateValueHorizontal * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
            }
        }
        else
        {
            if (curHorSpeed >= 0)
            {
                if (curHorInput < 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += accelerateValueHorizontal * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
                else
                {
                    curHorSpeed += accelerateValueHorizontal * curHorInput * Time.fixedDeltaTime;
                }
            }
            else if (curHorSpeed <= 0)
            {
                if (curHorInput > 0)    // let the player contribute to slowing down
                {
                    curHorSpeed += accelerateValueHorizontal * movementCancelHelper * curHorInput * Time.fixedDeltaTime;
                }
                else
                {
                    curHorSpeed += accelerateValueHorizontal * curHorInput * Time.fixedDeltaTime;
                }
            }
        }
    }

    private void AirBrakeLogic()
    {
        if (airBrakeQueued)
        {
            if (_hookcontroller.IsConnected())
                _hookcontroller.DisconnectHook();
            StartCoroutine(DisableAirMovement(airBrakeAirDisableTime));
            StartCoroutine(AirBrakeModifier(airBrakeModifierTime));

            AudioManager.instance.PlaySound("AirBrake");

            rb.velocity = new Vector2(0f, 0f);
            ApplyJump(0, airBrakeJumpForceMult);
            airBrakeQueued = false;
        }
    }

    private void DashLogic()
    {
        if (dashQueued && _hookcontroller.IsConnected() && !IsGrounded())
        {
            Vector2 hookPosition = new Vector2(hookObject.transform.position.x, hookObject.transform.position.y);

            Vector2 dashDirection;
            if (transform.position.x < hookPosition.x)
            {
                if (rb.velocity.x < 0)
                {
                    dashDirection = -rb.velocity.normalized;
                }
                else
                {
                    dashDirection = rb.velocity.normalized;
                }
            }
            else if (transform.position.x > hookPosition.x)
            {
                if (rb.velocity.x > 0)
                {
                    dashDirection = -rb.velocity.normalized;
                }
                else
                {
                    dashDirection = rb.velocity.normalized;
                }
            }
            else
            {
                //If the player is stationary, just dash in the direction they are facing
                int facing = 0;
                if (directionFacing == HoriDirection.Right)
                {
                    facing = 1;
                }
                else
                {
                    facing = -1;
                }
                dashDirection = new Vector2(facing, 0);
            }

            int randomNum = UnityEngine.Random.Range(1, 4);
            AudioManager.instance.PlaySound("HookDash" + randomNum);

            rb.velocity = dashDirection * hookDashMoveMagnitude;
            dashParticleSystem.Play(false);
            dashQueued = false;
            dashBuffered = false;
        }
    }

    private void JumpLogic()
    {
        // Ground Jump
        if (jumpQueued && (!_hookcontroller.IsConnected() || IsGrounded()))
        {
            //Debug.Log("Regular Jump");
            jumpQueued = false;
            jumpBuffered = false;
            hangTimer = 0.0f;
            directionWhenJumpStarted = directionFacing;

            rb.velocity = new Vector2(rb.velocity.x, 0f);
            ApplyJump(0, jumpForce);
        }
        // Hook Jump
        else if (jumpQueued && _hookcontroller.IsConnected())
        {
            // Check if next to wall
            float distance = 0.03f;
            Vector3 castOrigin = bottomCollider.bounds.center;
            Vector2 castSize = new Vector2(bottomCollider.radius * 2f, bottomCollider.radius * 0.6f);
            RaycastHit2D raycastHit = Physics2D.BoxCast(castOrigin, castSize, 0f, new Vector2((int)directionFacing, 0f), distance, groundLayer);

            if (raycastHit.collider == null)
            {
                //Debug.Log("Hook Jump!");
                jumpQueued = false;
                DetachHook.Invoke();

                isAirJumping = true;
                directionWhenJumpStarted = directionFacing;

                rb.velocity = new Vector2(rb.velocity.x * hookJumpHorizMult, 0f);
                ApplyJump(0, hookJumpForceMult);
            }
            else
            {
                wallJumpQueued = true;
            }
        }

        // Wall Jump
        if (wallJumpQueued)
        {
            // Temporarily disable air movement
            StartCoroutine(DisableAirMovement(wallJumpAirDisableTime));
            wallJumpQueued = false;
            wallJumpBuffered = false;
            directionWhenJumpStarted = directionFacing;

            rb.velocity = new Vector2(0f, 0f);
            ApplyJump(-wallSide * wallJumpOffForce, jumpForce);
            isWallSliding = false;
            rb.gravityScale = rbDefaultGravityScale;
        }
    }

    private void ApplyJump(float xforce, float yforce)
    {
        rb.velocity += new Vector2(xforce, yforce);
        int randomNum = UnityEngine.Random.Range(1, 2);
        AudioManager.instance.PlaySound("Jump" + randomNum);
    }
    #endregion

    #region Hook Methods and Enums

    private void HookHitGround(HookSide hookSide)
    {
        // Cancel any jump-cancel that might have been happening
        isAirJumping = false;
    }

    private bool IsHooked()
    {
        return (_hookcontroller.HookOut && _hookcontroller.HookOnGround);
    }

    public static Vector2 SnapOctDirection(float horVal, float verValue)
    {
        Vector2 snappedDirection = new Vector2();
        if (horVal > deadZoneValue)
            snappedDirection.x = 1f;
        else if (horVal < -deadZoneValue)
            snappedDirection.x = -1f;
        if (verValue > deadZoneValue)
            snappedDirection.y = 1f;
        else if (verValue < -deadZoneValue)
            snappedDirection.y = -1f;
        return snappedDirection.normalized;
    }

    enum HookedState
    {
        None = 0,
        One = 1,
        Both = 2
    }

    enum HoriDirection
    {
        Left = -1,
        Right = 1
    }

    #endregion

    #region On-Screen Debug Text

    private GUIStyle bigFont = new GUIStyle();

    private void OnGUI()
    {
        if (DebugOptions.debugText)
        {
            bigFont.fontSize = 10;
            bigFont.normal.textColor = Color.white;

            GUI.Label(new Rect(10, 20, 1000, 1000),
                  "OnGround? " + IsGrounded() +
                "\nHook Out?" + _hookcontroller.HookOut +
                "\nHook Gnd?" + _hookcontroller.HookOnGround +
                "\nIsHooked? " + IsHooked() +
                "\nW. Slide? " + isWallSliding +
                "\nWJmpBuff? " + jumpBuffered +
                "\nIsOnWall? " + isOnWall +
                //"\nSpeed w wall? " + (curHorInput * wallSide) +
                "\nHasWallBehind? " + hasWallBehind +
                "\nDash Queued? " + dashQueued +
                "\nAirB Queued? " + airBrakeQueued +
                "\nIsAirBraking? " + isAirBraking +
                "\nGndAirBraking? " + groundAirBraking +
                "\nAllowAirBraking ? " + allowAirBraking +
                "\nJumpBuff? " + jumpBuffered +
                "\nwallHangState? " + wallHangState
                //"\ncurHorSpeed? " + curHorSpeed +
                //"\ncurHorInput? " + curHorInput
                //"\nHookR_Connected: " + hookR_connected
                ,
                bigFont);
        }
    }

    #endregion

}
