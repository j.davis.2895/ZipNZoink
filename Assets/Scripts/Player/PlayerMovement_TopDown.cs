﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerMovement_TopDown : MonoBehaviour
{
    #region Variables

    public static UnityEvent DetachHook;

    [Header("Hook Setup")]
    public GameObject hookObject;     // The hook head for the right hook.
    private HookController_TopDown _hookcontroller;
    public HookController_TopDown HookController
    {
        get { return _hookcontroller; }
        private set { }
    }
    public HookControllerCommonSetup commonHookData;    // Common data for hook setup
    //private bool hookR_connected = false;

    /*** MOVEMENT DATA ***/
    private Rigidbody2D rb;
    [SerializeField] private CircleCollider2D bottomCollider = new CircleCollider2D();
    [SerializeField] private LayerMask groundLayer = new LayerMask();


    [Header("Movement Helpers")]
    private float hookSwingToApply;
    private HoriDirection directionFacing;    // -1 is left, 1 is right
    private HoriDirection directionWhenJumpStarted;
    private float rbDefaultGravityScale;
    private static float deadZoneValue = 0.1f;
    //TODO: Need to investigate how this works with air movement

    [Header("Input Containers")]
    private float curHorInput = 0;
    private float curVerInput = 0;  // One Hook Variant
    private float reelRightHook = 0;

    [Header("Visual Data")] // Most of this should eventually just be handles by an animator
    public GameObject lineRenderContainer;
    private SpriteRenderer playerSprite;
    public VisualAimAssist aimAssist;
    public ParticleSystem dashParticleSystem;
    public AirBrakeVisualHelper airBrakeVisuals;

    // NEW INPUT SYSTEM
    PlayerControls controls;
    #endregion

    #region Initialization Methods

    private void Awake()
    {
        if (DetachHook == null) DetachHook = new UnityEvent();

        _hookcontroller = this.gameObject.GetComponent<HookController_TopDown>();
        _hookcontroller.SetLineContainer(lineRenderContainer);
        _hookcontroller.SetupHook(hookObject, commonHookData);

        rb = GetComponent<Rigidbody2D>();
        rbDefaultGravityScale = rb.gravityScale;

        playerSprite = this.GetComponentInChildren<SpriteRenderer>();

        // New input system
        controls = new PlayerControls();
    }

    private void OnEnable()
    {
        HookHelper.OnHookHitGround += HookHitGround;

        controls.OneHook.HoriztonalAxis.performed += HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed += HandleVerticalaxis;
        controls.OneHook.Fire.performed += HandleFire;
        controls.OneHook.Reel.performed += HandleReel;

        controls.OneHook.HoriztonalAxis.Enable();
        controls.OneHook.VerticalAxis.Enable();
        controls.OneHook.Jump.Enable();
        controls.OneHook.Fire.Enable();
        controls.OneHook.Reel.Enable();
        controls.OneHook.HookDash.Enable();
        controls.OneHook.AirBrake.Enable();
    }

    private void OnDisable()
    {
        HookHelper.OnHookHitGround -= HookHitGround;

        controls.OneHook.HoriztonalAxis.performed -= HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed -= HandleVerticalaxis;
        controls.OneHook.Fire.performed -= HandleFire;
        controls.OneHook.Reel.performed -= HandleReel;

        controls.OneHook.HoriztonalAxis.Disable();
        controls.OneHook.VerticalAxis.Disable();
        controls.OneHook.Jump.Disable();
        controls.OneHook.Fire.Disable();
        controls.OneHook.Reel.Disable();
        controls.OneHook.HookDash.Disable();
        controls.OneHook.AirBrake.Disable();
    }

    private void Start()
    {
    }
    #endregion

    #region Input Handlers
    private void HandleHorizontalAxis(InputAction.CallbackContext obj)
    {
        curHorInput = obj.ReadValue<float>();

        if (Mathf.Abs(curHorInput) < deadZoneValue)
        {
            curHorInput = 0;
        }
    }

    private void HandleVerticalaxis(InputAction.CallbackContext obj)
    {
        curVerInput = obj.ReadValue<float>();
    }

    private void HandleReel(InputAction.CallbackContext obj)
    {
        reelRightHook = obj.ReadValue<float>();
    }

    private void HandleFire(InputAction.CallbackContext obj)
    {
        if (aimAssist.CurrentAimingData.validTarget)
        {
            Vector2 firingDirection = aimAssist.CurrentAimingData.targetPosition - this.transform.position;
            _hookcontroller.FireHook(firingDirection.normalized);
        }
        else
        {
            _hookcontroller.FireHook(SnapOctDirection(curHorInput, curVerInput));
        }
    }
    #endregion

    private void Update()
    {
        ControlHooks();
    }

    public bool IsGrounded()
    {
        float extraHeight = 0.03f;
        Vector3 castOrigin = bottomCollider.bounds.center;
        Vector2 castSize = new Vector2(bottomCollider.radius * 2, bottomCollider.radius * 2);
        RaycastHit2D raycastHit = Physics2D.BoxCast(castOrigin, castSize, 0f, Vector2.down, extraHeight, groundLayer);

        //One Way Platform Conditional: Make sure the player is ABOVE the platform before IsGrounded() is true
        if (raycastHit.collider != null && raycastHit.collider.TryGetComponent(out PlatformEffector2D platEffector))
        {
            if (platEffector.useOneWay && rb.velocity.y > 0.1f || this.transform.position.y - 0.5f < raycastHit.point.y)
            {
                return false;
            }
        }

        return raycastHit.collider != null;
    }

    private void ControlHooks()
    {
        //TODO: Stephen: I think we should check the hook state using public bools from Hook Controller, rather than a local variable that gets toggled
        //hookR_connected = hookR_controller.HookOnGround;

        _hookcontroller.ReelHook(reelRightHook);
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector2(curHorInput * Time.fixedDeltaTime * 5f, curVerInput * Time.fixedDeltaTime * 5f));
    }

    #region FixedUpdate Movement Methods

    private void ApplyJump(float xforce, float yforce)
    {
        rb.velocity += new Vector2(xforce, yforce);
    }
    #endregion

    #region Hook Methods and Enums

    private void HookHitGround(HookSide hookSide)
    {
        // Cancel any jump-cancel that might have been happening
    }

    private bool IsHooked()
    {
        return (_hookcontroller.HookOut && _hookcontroller.HookOnGround);
    }

    public static Vector2 SnapOctDirection(float horVal, float verValue)
    {
        Vector2 snappedDirection = new Vector2();
        if (horVal > deadZoneValue)
            snappedDirection.x = 1f;
        else if (horVal < -deadZoneValue)
            snappedDirection.x = -1f;
        if (verValue > deadZoneValue)
            snappedDirection.y = 1f;
        else if (verValue < -deadZoneValue)
            snappedDirection.y = -1f;
        return snappedDirection.normalized;
    }

    enum HookedState
    {
        None = 0,
        One = 1,
        Both = 2
    }

    enum HoriDirection
    {
        Left = -1,
        Right = 1
    }

    #endregion

    #region On-Screen Debug Text

    private GUIStyle bigFont = new GUIStyle();

    private void OnGUI()
    {
        if (DebugOptions.debugText)
        {
            bigFont.fontSize = 10;
            bigFont.normal.textColor = Color.white;

            GUI.Label(new Rect(10, 20, 1000, 1000),
                "\nHook Out?" + _hookcontroller.HookOut +
                "\nHook Gnd?" + _hookcontroller.HookOnGround +
                "\nIsHooked? " + IsHooked()
                //"\nSpeed w wall? " + (curHorInput * wallSide) +
                //"\ncurHorSpeed? " + curHorSpeed +
                //"\ncurHorInput? " + curHorInput
                //"\nHookR_Connected: " + hookR_connected
                ,
                bigFont);
        }
    }

    #endregion

}
