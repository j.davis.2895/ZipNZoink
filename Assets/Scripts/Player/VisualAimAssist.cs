﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VisualAimAssist : MonoBehaviour
{
    // Line Setup
    public Color rightColor;
    public Color leftColor;
    public Material lineMaterial;
    private float fadeDistance = 3.5f;
    private float lineWidth = 0.07f;
    private int layerOrder = -1;

    private LineRenderer rightLine;
    private LineRenderer leftLine;

    private Gradient rightGradient;
    private GradientColorKey[] rightColorKey;
    private Gradient leftGradient;
    private GradientColorKey[] leftColorKey;
    public GradientAlphaKey[] sharedAlphaKey;

    // Auto aim assisting
    PlayerControls controls;
    public PlayerMovement_OneHook movementController;
    float horizontalInput;
    float verticalInput;
    int groundedAssistLimit = 4;
    int groundedAssistDonwardsLimit = 2;
    Vector3 targetPreviewDirection;
    Vector2 aimingDirection;
    private AimingData currentAimingData;
    public AimingData CurrentAimingData
    {
        get {return currentAimingData; }
        private set { }
    }

    [Header("Aim Assist")]
    public LayerMask castingLayers;
    public float castDistance;
    public List<float> circleSizes = new List<float>();

    // Debug Visuals
    Vector3 debugCircleOrigin = new Vector3();
    float debugCircleSize = 0;
    bool drawDebugCircle = false;

    private void Awake()
    {
        // New input system
        controls = new PlayerControls();
    }

    private void OnEnable()
    {
        controls.OneHook.HoriztonalAxis.performed += HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed += HandleVerticalaxis;

        controls.OneHook.HoriztonalAxis.Enable();
        controls.OneHook.VerticalAxis.Enable();
    }

    private void OnDisable()
    {
        controls.OneHook.HoriztonalAxis.performed -= HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed -= HandleVerticalaxis;

        controls.OneHook.HoriztonalAxis.performed -= HandleHorizontalAxis;
        controls.OneHook.VerticalAxis.performed -= HandleVerticalaxis;
    }

    private void HandleHorizontalAxis(InputAction.CallbackContext obj)
    {
        horizontalInput = obj.ReadValue<float>();
    }

    private void HandleVerticalaxis(InputAction.CallbackContext obj)
    {
        verticalInput = obj.ReadValue<float>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rightGradient = new Gradient();
        rightColorKey = new GradientColorKey[2];
        rightColorKey[0].color = rightColor;
        rightColorKey[0].time = 0f;
        rightColorKey[1].color = Color.clear;
        rightColorKey[1].time = fadeDistance;

        leftGradient = new Gradient();
        leftColorKey = new GradientColorKey[2];
        leftColorKey[0].color = leftColor;
        leftColorKey[0].time = 0f;
        leftColorKey[1].color = Color.clear;
        leftColorKey[1].time = fadeDistance;

        sharedAlphaKey = new GradientAlphaKey[2];
        sharedAlphaKey[0].alpha = 1f;
        sharedAlphaKey[0].time = 0f;
        sharedAlphaKey[1].alpha = 0f;
        sharedAlphaKey[1].time = fadeDistance;

        rightGradient.SetKeys(rightColorKey, sharedAlphaKey);
        leftGradient.SetKeys(leftColorKey, sharedAlphaKey);

        GameObject rightLineContainer = new GameObject();
        rightLineContainer.transform.SetParent(this.transform);
        rightLine = rightLineContainer.AddComponent<LineRenderer>();
        rightLine.material = lineMaterial;
        rightLine.colorGradient = rightGradient;
        rightLine.startWidth = rightLine.endWidth = lineWidth;
        rightLine.sortingOrder = layerOrder;

        GameObject leftLineContainer = new GameObject();
        leftLineContainer.transform.SetParent(this.transform);
        leftLine = leftLineContainer.AddComponent<LineRenderer>();
        leftLine.material = lineMaterial;
        leftLine.colorGradient = leftGradient;
        leftLine.startWidth = leftLine.endWidth = lineWidth;
        leftLine.sortingOrder = layerOrder;
    }

    // Update is called once per frame
    void Update()
    {
        if (DebugOptions.hookFireVarient == HookFireVariant.OneHook)// && PlayerMovement_v3.SnapOctDirection(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) != Vector2.zero)
        {
            rightLine.enabled = false;
            leftLine.enabled = false;

            aimingDirection = PlayerMovement_OneHook.SnapOctDirection(horizontalInput, verticalInput);
            // Reflect if wall sliding
            if (movementController.IsWallSliding)
                aimingDirection.x *= -1;

            if (aimingDirection != Vector2.zero)
            {
                // Default
                targetPreviewDirection = (Vector3)aimingDirection;

                // Check for something to hit
                currentAimingData = GetTargetPosition();
                if (currentAimingData.validTarget && !movementController.HookController.IsConnected())
                {
                    targetPreviewDirection = currentAimingData.targetPosition - this.transform.position;
                    leftLine.enabled = true;
                    leftLine.SetPosition(0, currentAimingData.targetPosition);
                    leftLine.SetPosition(1, currentAimingData.targetPosition + (targetPreviewDirection.normalized * fadeDistance * -0.5f));
                }

                // Show direction for player
                rightLine.enabled = true;
                rightLine.SetPosition(0, this.transform.position);
                rightLine.SetPosition(1, this.transform.position + (targetPreviewDirection.normalized * fadeDistance));
            }
        }
        else if (DebugOptions.hookFireVarient != HookFireVariant.None)
        {
            rightLine.enabled = true;
            rightLine.SetPosition(0, this.transform.position);
            rightLine.SetPosition(1, this.transform.position + (new Vector3(1f, 1f, 0f) * fadeDistance));

            leftLine.SetPosition(0, this.transform.position);
            leftLine.SetPosition(1, this.transform.position + (new Vector3(-1f, 1f, 0f) * fadeDistance));
        }
    }

    public AimingData GetTargetPosition()
    {
        Vector3 targetPosition = new Vector3(0,0,0);
        bool validTarget = false;
        int iterationCounter = 0;
        bool playerIsGrounded = movementController.IsGrounded();

        foreach(float castSize in circleSizes)
        {
            // Limit aiming if player is on the ground, stop aim assist if true
            if (playerIsGrounded && iterationCounter >= groundedAssistLimit)
            {
                validTarget = false;
                break;
            }
            // When not aiming upwards (no accuracy needed because its aiming for the floor underneath)
            else if (playerIsGrounded && iterationCounter >= groundedAssistDonwardsLimit && aimingDirection.normalized.y < 0.1f)
            {
                validTarget = false;
                break;
            }

            Vector3 aimStartOffset = this.transform.position + (1f * castSize * (Vector3)aimingDirection.normalized);
            RaycastHit2D castHit = Physics2D.CircleCast(aimStartOffset, castSize, aimingDirection, castDistance, castingLayers);
            
            /*// Debug drawing circles
            debugCircleOrigin = aimStartOffset;
            debugCircleSize = castSize;
            drawDebugCircle = true; //*/
            
            if (castHit.collider != null)
            {
                // The following if statement makes sure that the first thing the cast hits is on the ground layer
                // If the thing we hit isn't the ground layer, we try again in the next cast first before breaking out
                if (castHit.collider.CompareTag("CameraEdge") || castHit.collider.CompareTag("Hazard"))
                {
                    validTarget = false;
                }
                else
                {
                    targetPosition = castHit.point;
                    validTarget = true;
                    break;
                }
            }

            iterationCounter++;
        }

        return new AimingData(targetPosition, validTarget);
    }

    private void OnDrawGizmos()
    {
        if (drawDebugCircle)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(debugCircleOrigin, debugCircleSize);
        }
    }

    public struct AimingData
    {
        public AimingData(Vector3 newTargetPosition, bool newValidTarget)
        {
            targetPosition = newTargetPosition;
            validTarget = newValidTarget;
        }

        public Vector3 targetPosition;
        public bool validTarget;
    }
}
