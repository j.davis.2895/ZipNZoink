﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugOptions
{
    public static HookFireVariant hookFireVarient;
    public static bool hookJump;
    public static bool debugText;
    public static bool usingController;
}

public enum HookFireVariant
{
    TwoPress = 0,
    Hold = 1,
    OnePress = 2,
    OneHook = 3,
    None = 4
}
