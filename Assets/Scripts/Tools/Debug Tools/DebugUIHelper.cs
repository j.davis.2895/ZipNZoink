﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class DebugUIHelper : MonoBehaviour
{
    public GameObject debugMenu;
    public float returningTimescale = 1f;

    //public Slider hookVariantSlider;
    //public Toggle hookJumpToggle;
    public Toggle debugTextToggle;
    public Toggle usingControllerToggle;
    public Button restartButton;
    private List<string> listOfLevels = new List<string>()
    {
        "Tutorial",
        "StaticSpikesLevel",
        "AdvStaticSpikesLevel",
        "RailsStaticSpikesLevel",
        "level_1",
        "level_2",
        "level_3",
        "level_4",
        "level_5",
        "level_6",
        "level_7",
        "level_8",
        "level_9"
    };

    PlayerControls controls;

    public static event Action OnChangeInputSource;

    private void Awake()
    {
        // New input system
        controls = new PlayerControls();
    }

    private void OnEnable()
    {
        controls.UI.Menu.performed += HandleMenu;

        controls.UI.Menu.Enable();
    }

    private void OnDisable()
    {
        controls.UI.Menu.performed -= HandleMenu;

        controls.UI.Menu.Disable();
    }

    private void HandleMenu(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        debugMenu.SetActive(!debugMenu.activeSelf);
        Time.timeScale = debugMenu.activeSelf ? 0f : returningTimescale;

        // Highlight/Select when paused
        if (debugMenu.activeSelf)
        {
            GameObject tempSel = EventSystem.current.currentSelectedGameObject;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(tempSel);
        }
    }

    void Start()
    {
        // Initialisation
        DebugUIController.instance.UpdateDebugOptions();
        EventSystem.current.SetSelectedGameObject(restartButton.gameObject);

        // Set UI based on current session's values
        //hookVariantSlider.normalizedValue = (float)DebugUIController.instance.hookFireVar - 0.5f;   // the 0.5 is needed otherwise it just rounds to the next highest/lowest value???
        //hookJumpToggle.isOn = DebugUIController.instance.hookJump;
        debugTextToggle.isOn = DebugUIController.instance.debugText;
        usingControllerToggle.isOn = DebugUIController.instance.usingController;

        debugMenu.SetActive(false);
    }

    /*
    private void Update()
    {
        if (InputManager.instance.MenuInput)
        {
            debugMenu.SetActive(!debugMenu.activeSelf);
            Time.timeScale = debugMenu.activeSelf ? 0f : 1f;

            // Highlight/Select when paused
            if (debugMenu.activeSelf)
            {
                GameObject tempSel = EventSystem.current.currentSelectedGameObject;
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(tempSel);
            }
        }
    }
    */

    // ADD NEW FUNCTIONS BELOW
    #region UI FUNCTIONS

    public void ChangeHookFireVariant(float variantNumber)
    {
        DebugUIController.instance.hookFireVar = (HookFireVariant)variantNumber;

        if (variantNumber == 2f)
        {
            DebugUIController.instance.hookJump = true;
            //hookJumpToggle.isOn = true;
        }
    }

    public void ChangeHookJumpEnabled(bool hookJumpNewState)
    {
        DebugUIController.instance.hookJump = hookJumpNewState;
    }

    public void ChangeDebugTextEnabled(bool debugTextNewState)
    {
        DebugUIController.instance.debugText = debugTextNewState;
    }

    public void ChangeUsingController(bool debugUsingControllerNewState)
    {
        DebugUIController.instance.usingController = debugUsingControllerNewState;
        OnChangeInputSource?.Invoke();
    }

    public void RestartLevel()
    {
        Time.timeScale = returningTimescale;
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //GameManager.instance.LoadLevel(SceneManager.GetActiveScene().name);
        GameManager.instance.RestartLevel();
    }

    public void NextLevel()
    {
        Time.timeScale = returningTimescale;
        int nextIndex = listOfLevels.IndexOf(SceneManager.GetActiveScene().name) + 1;
        if (nextIndex >= listOfLevels.Count - 1)
            nextIndex = listOfLevels.Count - 1;
        //SceneManager.LoadScene(listOfLevels[nextIndex]);
        GameManager.instance.LoadLevel(listOfLevels[nextIndex]);
    }

    public void PrevLevel()
    {
        Time.timeScale = returningTimescale;
        int prevIndex = listOfLevels.IndexOf(SceneManager.GetActiveScene().name) - 1;
        if (prevIndex <= 0)
            prevIndex = 0;
        //SceneManager.LoadScene(listOfLevels[prevIndex]);
        GameManager.instance.LoadLevel(listOfLevels[prevIndex]);
    }

    public void MainMenu()
    {
        Time.timeScale = returningTimescale;
        //SceneManager.LoadScene("MainMenu");
        GameManager.instance.LoadLevel("MainMenu");
    }

    public void IncreaseTimeScale()
    {
        returningTimescale += 0.1f;
        Debug.Log(returningTimescale);
    }

    public void DecreaseTimeScale()
    {
        returningTimescale -= 0.1f;
        Debug.Log(returningTimescale);
    }

    #endregion
}
