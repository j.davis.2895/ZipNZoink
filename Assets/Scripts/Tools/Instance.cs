﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Instance
{
    //public static int cameraIndex = -1;
    public static string currentCameraName;

    public static Vector3 currentPosition;

    public static string currentLevel;

    public static Dictionary<string, List<bool>> pickupState = new Dictionary<string, List<bool>>();

    public static Dictionary<string, List<bool>> checkPoints = new Dictionary<string, List<bool>>();

}
