﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectWatcherScript: MonoBehaviour
{
    public GameObject startingCamera;
    public List<GameObject> cameras;
    public GameObject[] pickups;
    public static Dictionary<string, GameObject> camerasDict = new Dictionary<string, GameObject>();
}
