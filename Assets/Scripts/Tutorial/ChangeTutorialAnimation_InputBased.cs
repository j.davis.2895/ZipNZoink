﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTutorialAnimation_InputBased : MonoBehaviour
{
    public GameObject controllerVersion;
    public GameObject keyboardVersion;

    private void OnEnable()
    {
        DebugUIHelper.OnChangeInputSource += ChangeAnimation;
    }

    private void OnDisable()
    {
        DebugUIHelper.OnChangeInputSource -= ChangeAnimation;
    }

    private void Start()
    {
        ChangeAnimation();
    }

    private void ChangeAnimation()
    {
        if (DebugOptions.usingController)
        {
            controllerVersion.SetActive(true);
            keyboardVersion.SetActive(false);
        }
        else
        {
            controllerVersion.SetActive(false);
            keyboardVersion.SetActive(true);
        }
    }
}
