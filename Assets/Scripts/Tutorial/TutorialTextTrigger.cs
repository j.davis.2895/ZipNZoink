﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialTextTrigger : MonoBehaviour
{
    public SpriteRenderer ControllerSpriteRenderer;
    public SpriteRenderer KeyboardSpriteRenderer;
    public TextMeshProUGUI intructionText;

    public AnimationCurve fadeInCurve;
    public float fadeInSpeedMult = 2f;

    private Color spriteColor;
    private Color textColor;

    private bool playedOnce = false;

    void Start()
    {
        spriteColor = ControllerSpriteRenderer.color;
        textColor = intructionText.color;

        spriteColor.a = 0;
        textColor.a = 0;

        SetSpritesColor(spriteColor);
        //animatedSpriteRenderer_Controller.color = spriteColor;
        intructionText.color = textColor;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!playedOnce && collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(FadeIn());
            playedOnce = true;
        }
    }

    private IEnumerator FadeIn()
    {
        //Color spriteColor = animatedSpriteRenderer.color;
        //Color textColor = intructionText.color;
        float t = 0;

        while (t < 1)
        {
            spriteColor.a = fadeInCurve.Evaluate(t);
            textColor.a = fadeInCurve.Evaluate(t);

            //animatedSpriteRenderer_Controller.color = spriteColor;
            SetSpritesColor(spriteColor);
            intructionText.color = textColor;

            t += fadeInSpeedMult * Time.deltaTime;
            yield return null;
        }
    }

    private void SetSpritesColor(Color newColor)
    {
        ControllerSpriteRenderer.color = newColor;
        KeyboardSpriteRenderer.color = newColor;
    }
}
